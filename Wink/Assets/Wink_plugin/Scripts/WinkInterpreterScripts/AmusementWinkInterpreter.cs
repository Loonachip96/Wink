﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Wink;
using System.Collections.ObjectModel;

public class AmusementWinkInterpreter : WinkGestureInterpreter
{

    protected override void OnBufferFilled()
    {
        string thisColliderName     = colliderName;
        headOffsetOfThisObject.y    = 1f;

        ////////////////////// REQUIREMENTS TO OCCUR //////////////////////
        double minimalSideChangeAngleDistance = 20;
        double minimalPercentageTimeToLookAt = 80;
        double minimalFaceFixation = 0.9;
        ///////////////////////////////////////////////////////////////////

        Transform thisTransform = this.GetComponent<Transform>();
        StatisticVector<double> deflFB;
        StatisticVector<double> deflLR;
        StatisticVector<double> rot;
        double faceFixation;
        double percentageTimeGazingOnThisObject;


        // collect needed data
        deflFB                              = getStatisticsOfHeadRotationAroundAxisXRelativeTo(thisTransform);
        deflLR                              = getStatisticsOfHeadRotationAroundAxisZ();
        rot                                 = getStatisticsOfHeadRotationAroundAxisYRelativeTo(thisTransform);
        faceFixation                        = getHeadFixation();
        percentageTimeGazingOnThisObject    = getPercentageTimeGazingObjectWithName(thisColliderName);

        // make decisions
        bool playerMainlyLooksOnThisObject = percentageTimeGazingOnThisObject > minimalPercentageTimeToLookAt;
        bool hasHighFaceFixation = faceFixation > minimalFaceFixation;
        bool headFlippedLR = 
            (deflLR.min < 0) != (deflLR.max < 0) 
            && (Mathf.Abs((float)(deflLR.min - deflLR.max)) > minimalSideChangeAngleDistance);
        bool headFlippedFB = 
            (deflFB.min < 0) != (deflFB.max < 0) 
            && (Mathf.Abs((float)(deflFB.min - deflFB.max)) > minimalSideChangeAngleDistance);
        bool headFlippedRot = 
            (rot.min < 0) != (rot.max < 0) 
            && (Mathf.Abs((float)(rot.min - rot.max)) > minimalSideChangeAngleDistance);


        if (
            (headFlippedLR || headFlippedFB || headFlippedRot)
            && hasHighFaceFixation
            && playerMainlyLooksOnThisObject
        )
        {
            Debug.Log("Amusement detected!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            this.transform.Translate(new Vector3(0, 15f, 0));
        }
    }

    protected override void OnSampleFilled(WinkDataEntity sample)
    {
    }
}