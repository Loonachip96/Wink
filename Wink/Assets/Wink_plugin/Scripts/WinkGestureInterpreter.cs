﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Text;

namespace Wink
{
    abstract public class WinkGestureInterpreter : WinkInterface
    {
        protected string colliderName;
        protected Vector3 headOffsetOfThisObject;

        protected ReadOnlyCollection<WinkDataEntity> sampleBuffer;
        protected int samplesAmount;

        protected abstract void OnBufferFilled();
        protected abstract void OnSampleFilled(WinkDataEntity sample);

        private void TriggerBufferFilledEvent(ReadOnlyCollection<WinkDataEntity> buffer)
        {
            sampleBuffer = buffer;
            samplesAmount = sampleBuffer.Count;
            OnBufferFilled();
        }
        
        private void OnEnable()
        {
            WinkDataProcessor.OnBufferFilled += TriggerBufferFilledEvent;
            WinkDataProcessor.OnSampleCaptured += OnSampleFilled;
            Collider thisCollider = this.GetComponent<Collider>();

            if (thisCollider != null)
                colliderName = thisCollider.name;
            else
                colliderName = "";

            headOffsetOfThisObject = new Vector3(0, 0, 0);
        }

        private void OnDisable()
        {
            WinkDataProcessor.OnBufferFilled -= TriggerBufferFilledEvent;
            WinkDataProcessor.OnSampleCaptured -= OnSampleFilled;
        }

        private List<string> getDistinctNames(VarName varName)
        {
            List<string> uniques = new List<string>();


            if (varName == VarName.faceRaycastHitObjectName)
            {
                bool isUnique = true;

                for (int sample = 0; sample < sampleBuffer.Count; sample++)
                {
                    for (int u = 0; u < uniques.Count; u++)
                    {
                        if (sampleBuffer[sample].faceRaycastHitObjectName == uniques[u])
                        {
                            isUnique = false;
                            break;
                        }
                    }
                    if (isUnique)
                        uniques.Add(sampleBuffer[sample].faceRaycastHitObjectName);
                }
            }
            else if (varName == VarName.gazeRaycastHitObjectName)
            {
                bool isUnique = true;

                for (int sample = 0; sample < sampleBuffer.Count; sample++)
                {
                    for (int u = 0; u < uniques.Count; u++)
                    {
                        if (sampleBuffer[sample].gazeRaycastHitObjectName == uniques[u])
                        {
                            isUnique = false;
                            break;
                        }
                    }
                    if (isUnique)
                        uniques.Add(sampleBuffer[sample].gazeRaycastHitObjectName);
                }
            }
            else
                return null;

            return uniques;
        }

        public StatisticVector<double> getStatisticsOfDeltaTime()
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                if (sample.deltaTime > stats.max)
                    stats.max = sample.deltaTime;
                if (sample.deltaTime < stats.min)
                    stats.min = sample.deltaTime;
                stats.avg += sample.deltaTime;
            }
            stats.avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
                stats.stdDev += Mathf.Abs((float)(sample.deltaTime - stats.avg));

            stats.stdDev /= sampleCount;

            return stats;
        }

        public StatisticVector<double> getStatisticsOfCalibrationConfidence()
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                if (sample.calibrationConfidence > stats.max)
                    stats.max = sample.calibrationConfidence;
                if (sample.calibrationConfidence < stats.min)
                    stats.min = sample.calibrationConfidence;
                stats.avg += sample.calibrationConfidence;
            }
            stats.avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
                stats.stdDev += Mathf.Abs((float)(sample.calibrationConfidence - stats.avg));

            stats.stdDev /= sampleCount;

            return stats;
        }

        public StatisticVector<double> getStatisticsOfGazingUncertainty()
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                if (sample.avgGazingUncertainty.x > stats.max)
                    stats.max = sample.avgGazingUncertainty.x;
                if (sample.avgGazingUncertainty.x < stats.min)
                    stats.min = sample.avgGazingUncertainty.x;
                if (sample.avgGazingUncertainty.x > stats.max)
                    stats.max = sample.avgGazingUncertainty.x;
                if (sample.avgGazingUncertainty.x < stats.min)
                    stats.min = sample.avgGazingUncertainty.x;

                if (sample.avgGazingUncertainty.y > stats.max)
                    stats.max = sample.avgGazingUncertainty.y;
                if (sample.avgGazingUncertainty.y < stats.min)
                    stats.min = sample.avgGazingUncertainty.y;
                if (sample.avgGazingUncertainty.y > stats.max)
                    stats.max = sample.avgGazingUncertainty.y;
                if (sample.avgGazingUncertainty.y < stats.min)
                    stats.min = sample.avgGazingUncertainty.y;

                stats.avg += sample.avgGazingUncertainty.x;
                stats.avg += sample.avgGazingUncertainty.y;
            }

            stats.avg /= 2 * sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                stats.stdDev += Mathf.Abs((float)(sample.avgGazingUncertainty.x - stats.avg));
                stats.stdDev += Mathf.Abs((float)(sample.avgGazingUncertainty.y - stats.avg));
            }

            stats.stdDev /= 2 * sampleCount;

            return stats;
        }

        public StatisticVector<double>[] getStatisticsOfGazePosition()
        {
            StatisticVector<double>[] stats = new StatisticVector<double>[2];
            int sampleCount = sampleBuffer.Count;

            stats[0] = new StatisticVector<double>(0);
            stats[1] = new StatisticVector<double>(0);


            foreach (WinkDataEntity sample in sampleBuffer)
            {
                if (sample.gazePosition.x > stats[0].max)
                    stats[0].max = sample.gazePosition.x;
                if (sample.gazePosition.x < stats[0].min)
                    stats[0].min = sample.gazePosition.x;
                if (sample.gazePosition.x > stats[0].max)
                    stats[0].max = sample.gazePosition.x;
                if (sample.gazePosition.x < stats[0].min)
                    stats[0].min = sample.gazePosition.x;

                if (sample.gazePosition.y > stats[1].max)
                    stats[1].max = sample.gazePosition.y;
                if (sample.gazePosition.y < stats[1].min)
                    stats[1].min = sample.gazePosition.y;
                if (sample.gazePosition.y > stats[1].max)
                    stats[1].max = sample.gazePosition.y;
                if (sample.gazePosition.y < stats[1].min)
                    stats[1].min = sample.gazePosition.y;

                stats[0].avg += sample.gazePosition.x;
                stats[1].avg += sample.gazePosition.y;
            }

            stats[0].avg /= sampleCount;
            stats[1].avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                stats[0].stdDev += Mathf.Abs((float)(sample.gazePosition.x - stats[0].avg));
                stats[1].stdDev += Mathf.Abs((float)(sample.gazePosition.y - stats[1].avg));
            }

            stats[0].stdDev /= sampleCount;
            stats[1].stdDev /= sampleCount;

            return stats;
        }

        public StatisticVector<double> getStatisticsOfGazeJumpsDistances()
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                if (sample.gazeJumpDistance > stats.max)
                    stats.max = sample.gazeJumpDistance;
                if (sample.gazeJumpDistance < stats.min)
                    stats.min = sample.gazeJumpDistance;

                stats.avg += sample.gazeJumpDistance;
            }

            stats.avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
                stats.stdDev += Mathf.Abs((float)(sample.gazeJumpDistance - stats.avg));

            stats.stdDev /= sampleCount;

            return stats;
        }

        public StatisticVector<double> getStatisticsOfGazeVelocity()
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                if (sample.gazeVelocity > stats.max)
                    stats.max = sample.gazeVelocity;
                if (sample.gazeVelocity < stats.min)
                    stats.min = sample.gazeVelocity;

                stats.avg += sample.gazeVelocity;
            }

            stats.avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
                stats.stdDev += Mathf.Abs((float)(sample.gazeVelocity - stats.avg));

            stats.stdDev /= sampleCount;

            return stats;
        }

        public List<Occurance> getOccurancesOfGazeRaycastHitObjectNames()
        {
            List<Occurance> occurances = new List<Occurance>();
            List<string> uniqueNames = getDistinctNames(VarName.gazeRaycastHitObjectName);

            for (int i = 0; i < uniqueNames.Count; i++)
                occurances.Add(new Occurance(uniqueNames[i], 0));

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                for (int i = 0; i < uniqueNames.Count; i++)
                {
                    if (uniqueNames[i] == sample.gazeRaycastHitObjectName)
                    {
                        occurances[i].occurance++;
                        break;
                    }
                }
            }

            return occurances;
        }

        public List<Occurance> getOccurancesOfFaceRaycastHitObjectNames()
        {
            List<Occurance> occurances = new List<Occurance>();
            List<string> uniqueNames = getDistinctNames(VarName.faceRaycastHitObjectName);

            for (int i = 0; i < uniqueNames.Count; i++)
                occurances.Add(new Occurance(uniqueNames[i], 0));

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                for (int i = 0; i < uniqueNames.Count; i++)
                {
                    if (uniqueNames[i] == sample.faceRaycastHitObjectName)
                    {
                        occurances[i].occurance++;
                        break;
                    }
                }
            }

            return occurances;
        }

        public StatisticVector<double> getStatisticsOfHeadRotationAroundAxisX()
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;


            foreach (WinkDataEntity sample in sampleBuffer)
            {
                if (sample.headRotationAroundAxis.x > stats.max)
                    stats.max = sample.headRotationAroundAxis.x;
                if (sample.headRotationAroundAxis.x < stats.min)
                    stats.min = sample.headRotationAroundAxis.x;
                if (sample.headRotationAroundAxis.x > stats.max)
                    stats.max = sample.headRotationAroundAxis.x;
                if (sample.headRotationAroundAxis.x < stats.min)
                    stats.min = sample.headRotationAroundAxis.x;

                stats.avg += sample.headRotationAroundAxis.x;
            }

            stats.avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
                stats.stdDev += Mathf.Abs((float)(sample.headRotationAroundAxis.x - stats.avg));

            stats.stdDev /= sampleCount;

            stats.avg = clipAngle(stats.avg);
            stats.max = clipAngle(stats.max);
            stats.min = clipAngle(stats.min);
            stats.stdDev = clipAngle(stats.stdDev);
            return stats;
        }

        public StatisticVector<double> getStatisticsOfHeadRotationAroundAxisY()
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;


            foreach (WinkDataEntity sample in sampleBuffer)
            {
                if (sample.headRotationAroundAxis.y > stats.max)
                    stats.max = sample.headRotationAroundAxis.y;
                if (sample.headRotationAroundAxis.y < stats.min)
                    stats.min = sample.headRotationAroundAxis.y;
                if (sample.headRotationAroundAxis.y > stats.max)
                    stats.max = sample.headRotationAroundAxis.y;
                if (sample.headRotationAroundAxis.y < stats.min)
                    stats.min = sample.headRotationAroundAxis.y;

                stats.avg += sample.headRotationAroundAxis.y;
            }

            stats.avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
                stats.stdDev += Mathf.Abs((float)(sample.headRotationAroundAxis.y - stats.avg));

            stats.stdDev /= sampleCount;

            stats.avg = clipAngle(stats.avg);
            stats.max = clipAngle(stats.max);
            stats.min = clipAngle(stats.min);
            stats.stdDev = clipAngle(stats.stdDev);
            return stats;
        }

        public StatisticVector<double> getStatisticsOfHeadRotationAroundAxisZ()
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;


            foreach (WinkDataEntity sample in sampleBuffer)
            {
                if (sample.headRotationAroundAxis.z > stats.max)
                    stats.max = sample.headRotationAroundAxis.z;
                if (sample.headRotationAroundAxis.z < stats.min)
                    stats.min = sample.headRotationAroundAxis.z;
                if (sample.headRotationAroundAxis.z > stats.max)
                    stats.max = sample.headRotationAroundAxis.z;
                if (sample.headRotationAroundAxis.z < stats.min)
                    stats.min = sample.headRotationAroundAxis.z;

                stats.avg += sample.headRotationAroundAxis.z;
            }

            stats.avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
                stats.stdDev += Mathf.Abs((float)(sample.headRotationAroundAxis.z - stats.avg));

            stats.stdDev /= sampleCount;

            stats.avg = clipAngle(stats.avg);
            stats.max = clipAngle(stats.max);
            stats.min = clipAngle(stats.min);
            stats.stdDev = clipAngle(stats.stdDev);
            return stats;
        }

        public StatisticVector<double> getStatisticsOfHeadRotationAroundAxisXRelativeTo(Transform objectTransform)
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                double relativeAngle = getRelativeHeadRotationAroundAxisX(sample, objectTransform);


                if (relativeAngle > stats.max)
                    stats.max = relativeAngle;
                if (relativeAngle < stats.min)
                    stats.min = relativeAngle;
                if (relativeAngle > stats.max)
                    stats.max = relativeAngle;
                if (relativeAngle < stats.min)
                    stats.min = relativeAngle;

                stats.avg += sample.headRotationAroundAxis.x;
            }

            stats.avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
                stats.stdDev += Mathf.Abs((float)(getRelativeHeadRotationAroundAxisX(sample, objectTransform) - stats.avg));

            stats.stdDev /= sampleCount;

            stats.avg = clipAngle(stats.avg);
            stats.max = clipAngle(stats.max);
            stats.min = clipAngle(stats.min);
            stats.stdDev = clipAngle(stats.stdDev);
            return stats;
        }

        public StatisticVector<double> getStatisticsOfHeadRotationAroundAxisYRelativeTo(Transform objectTransform)
        {
            StatisticVector<double> stats = new StatisticVector<double>(0);
            int sampleCount = sampleBuffer.Count;

            foreach (WinkDataEntity sample in sampleBuffer)
            {
                double relativeAngle = getRelativeHeadRotationAroundAxisY(sample, objectTransform);


                if (relativeAngle > stats.max)
                    stats.max = relativeAngle;
                if (relativeAngle < stats.min)
                    stats.min = relativeAngle;
                if (relativeAngle > stats.max)
                    stats.max = relativeAngle;
                if (relativeAngle < stats.min)
                    stats.min = relativeAngle;

                stats.avg += sample.headRotationAroundAxis.x;
            }

            stats.avg /= sampleCount;

            foreach (WinkDataEntity sample in sampleBuffer)
                stats.stdDev += Mathf.Abs((float)(getRelativeHeadRotationAroundAxisY(sample, objectTransform) - stats.avg));

            stats.stdDev /= sampleCount;

            stats.avg = clipAngle(stats.avg);
            stats.max = clipAngle(stats.max);
            stats.min = clipAngle(stats.min);
            stats.stdDev = clipAngle(stats.stdDev);
            return stats;
        }

        public double getRelativeHeadRotationAroundAxisX(WinkDataEntity sample, Transform objectTransform)
        {
            float distanceInZ, distanceInY, distanceInX;
            float distanceOnXZSurface;
            float correctionAngleOverXToFaceObject;

            distanceInZ = Mathf.Abs(objectTransform.position.z + headOffsetOfThisObject.z - sample.headPosition.z);
            distanceInY = Mathf.Abs(objectTransform.position.y + headOffsetOfThisObject.y - sample.headPosition.y);
            distanceInX = Mathf.Abs(objectTransform.position.x + headOffsetOfThisObject.x - sample.headPosition.x);
            distanceOnXZSurface = Mathf.Sqrt(distanceInX * distanceInX + distanceInZ * distanceInZ);

            correctionAngleOverXToFaceObject = 
                Mathf.Rad2Deg * Mathf.Atan( distanceInY / distanceOnXZSurface);

            if (objectTransform.position.y + headOffsetOfThisObject.y > sample.headPosition.y)
                correctionAngleOverXToFaceObject = -correctionAngleOverXToFaceObject;


            return sample.headRotationAroundAxis.x - correctionAngleOverXToFaceObject;
        }

        public double getRelativeHeadRotationAroundAxisY(WinkDataEntity sample, Transform objectTransform)
        {
            float distanceInZ, distanceInY, distanceInX;
            float distanceOnXZSurface;
            float correctionAngleOverYToFaceObject;
            bool objectInFront;

            distanceInZ = Mathf.Abs(objectTransform.position.z + headOffsetOfThisObject.z - sample.headPosition.z);
            distanceInY = Mathf.Abs(objectTransform.position.y + headOffsetOfThisObject.y - sample.headPosition.y);
            distanceInX = Mathf.Abs(objectTransform.position.x + headOffsetOfThisObject.x - sample.headPosition.x);
            distanceOnXZSurface = Mathf.Sqrt(distanceInX * distanceInX + distanceInZ * distanceInZ);

            objectInFront = (objectTransform.position.z + headOffsetOfThisObject.z > sample.headPosition.z);

            if (objectInFront)
                correctionAngleOverYToFaceObject = Mathf.Rad2Deg * Mathf.Atan(distanceInX / distanceInZ);
            else
                correctionAngleOverYToFaceObject = 90 + Mathf.Rad2Deg * (Mathf.PI / 2 - Mathf.Atan(distanceInX / distanceInZ));


            if (objectTransform.position.x + headOffsetOfThisObject.x < sample.headPosition.x)
                correctionAngleOverYToFaceObject = -correctionAngleOverYToFaceObject;

            return sample.headRotationAroundAxis.y + correctionAngleOverYToFaceObject;
        }

        public double getHeadFixation() {
            double faceFixation = 0;
            StatisticVector<double> deflFB = getStatisticsOfHeadRotationAroundAxisX();
            StatisticVector<double> rot = getStatisticsOfHeadRotationAroundAxisY();
            StatisticVector<double> deflLR = getStatisticsOfHeadRotationAroundAxisZ();

            faceFixation = 1 - (deflFB.stdDev/180 + deflLR.stdDev/180 + rot.stdDev/180) / 3;

            return faceFixation;
        }

        public double getGazeFixation()
        {
            StatisticVector<double>[] gazeStats = new StatisticVector<double>[2];

            gazeStats = getStatisticsOfGazePosition();

            return 1 - (gazeStats[0].stdDev + gazeStats[1].stdDev) / 2;
        }


        public double getPercentageTimeFacingObjectWithName(string colliderName)
        {
            List<Occurance> occurances;
            Occurance thisObjectFacingOccurance;

            occurances = getOccurancesOfFaceRaycastHitObjectNames();
            thisObjectFacingOccurance = occurances.Find(hit => hit.name == colliderName);
            if (thisObjectFacingOccurance != null)
                return (double)thisObjectFacingOccurance.occurance / samplesAmount * 100;
            else
                return 0;
        }

        public double getPercentageTimeGazingObjectWithName(string colliderName)
        {
            List<Occurance> occurances;
            Occurance thisObjectGazingOccurance;

            occurances = getOccurancesOfGazeRaycastHitObjectNames();
            thisObjectGazingOccurance = occurances.Find(hit => hit.name == colliderName);
            if (thisObjectGazingOccurance != null)
                return (double)thisObjectGazingOccurance.occurance / samplesAmount * 100;
            else
                return 0;
        }

    }
}