function [result] = getTimeAxisFromDeltas(deltaTimes)

    deltasAmount = size(deltaTimes, 1);
    result = zeros(deltasAmount, 1);
    for i=2:size(deltaTimes, 1)
        result(i) = sum(deltaTimes(1:i));
    end
    result(1) = 0;
    
end

