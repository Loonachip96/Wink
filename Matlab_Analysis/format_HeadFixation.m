function newBuffer = format_HeadFixation(lineBuffer, fixation)
    bufferLength = size(lineBuffer, 2);
    newBuffer = lineBuffer;
    
    lineString = 'Head Fixation:';
    newBuffer{bufferLength+1} = lineString(:);
    
    lineString = sprintf('   %f\n', fixation);
    newBuffer{bufferLength+2} = lineString(:);
    
end

