function [index] = findIndexOfFirstStringWithPrefix(strings, prefixes)
    index = -1;
    stringsAmount = size(strings, 1);
    prefixesAmount = size(prefixes, 2);
    
    for iPrefix= 1:prefixesAmount
        prefixLength = size(prefixes{iPrefix}, 2);
        for iString= 1:stringsAmount
            stringLength = size(strings(iString, :), 2);
            if(stringLength < prefixLength)
                continue;
            else
                if strcmp( strings(iString, 1:prefixLength), prefixes{iPrefix} )
                    index = iString;
                    objFound = true;
                    break;
                end
            end
        end
        if(index > 0)
            break;
        end
    end
end

