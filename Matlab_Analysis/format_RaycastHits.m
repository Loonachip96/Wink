function newBuffer = format_RaycastHits(lineBuffer, objectNames, percentageTime, header)
    newBuffer = lineBuffer;
    bufferLength = size(newBuffer, 2);
    
    newBuffer{bufferLength+1} = sprintf(header)';
    
    for hit=1:size(objectNames,1)
        hitString = sprintf('   %.6f%% ->  ', percentageTime(hit));
        hitString = strcat(hitString, objectNames(hit, :));
        bufferLength = size(newBuffer, 2);
        newBuffer{bufferLength+1} = hitString(:);
    end
    
end

