function newBuffer = format_GazingSpeed(lineBuffer, avg, maximum, minimum, stdDev, uncertainty)
    bufferLength = size(lineBuffer, 2);
    newBuffer = lineBuffer;
    
    lineString = sprintf('Avg. gazeTracking uncertainty:  %.6f\n', uncertainty);
    newBuffer{bufferLength+1} = lineString(:);
    lineString = 'Gaze speed parameters:';
    newBuffer{bufferLength+2} = lineString(:);
    
    lineString = sprintf('   std. deviat.:  %.6f\n', stdDev);
    newBuffer{bufferLength+3} = lineString(:);
    lineString = sprintf('   maximum:  %.6f\n', maximum);
    newBuffer{bufferLength+4} = lineString(:);
    lineString = sprintf('   average:  %.6f\n', avg);
    newBuffer{bufferLength+5} = lineString(:);
      
end

