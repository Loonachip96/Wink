function [] = printStatistics(data, comparementLevels, rowNames, maxValuesToShow)

[occurence, occurringValues, samplesLessThanOrEqualLevels, samplesGreaterOrEqualLevels] = occurenceVector(data, comparementLevels);
    
    for row = 1:size(data, 1)        
        fprintf('\t%s\n', rowNames{row});
        
        meanV = mean(data(row, :));
        fprintf('\t\tMean:\t%.6f\n', meanV);
        
        stdV = std(data(row, :));
        fprintf('\t\tStd:\t%.6f\n', stdV);
        
        modalV = mode(data(row, :), 'all');
        fprintf('\t\tModal:\t%.6f\n', modalV(:));
        
        minV = min(data(row, :));
        fprintf('\t\tMinimum:\t%.6f\n', minV(:));
        
        maxV = max(data(row, :));
        fprintf('\t\tMaximum:\t%.6f\n', maxV(:));
        
        if size(comparementLevels, 1) > 0
            fprintf('\t\tRelative percentage occurance:\n');
            for iLevel=1:size(comparementLevels{row}, 2)
                    level = comparementLevels{row}(iLevel);
                    occurences = samplesLessThanOrEqualLevels{row}(iLevel);
                    percent = occurences / size(data,2) * 100;
                    fprintf('\t\t\t %.2f%%\tvalues are <= %.6f\n', percent, level);
            end
            for iLevel=1:size(comparementLevels{row}, 2)
                    level = comparementLevels{row}(iLevel);
                    occurences = samplesGreaterOrEqualLevels{row}(iLevel);
                    percent = occurences / size(data,2) * 100;
                    fprintf('\t\t\t %.2f%%\tvalues are >= %.6f\n', percent, level);
            end
        end
        
        if (size(occurringValues{1, row}, 2) < maxValuesToShow)
            occurenceV = occurringValues{1, row}(:)';
            occurenceN = occurence{1,row}(:)';
        else
            fprintf('\t\t(All values occuring once are hidden)\n');
            occurenceV = occurringValues{1, row}(occurence{1, row}(:)>1);
            occurenceN = occurence{1,row}(occurence{1, row}(:)>1);
        end
        fprintf('\t\tOccurence of:\n');
        for value = 1:size(occurenceV, 2)
            fprintf('\t\t\t%.6f\tis\t%d\t out of\t%d\t(%.2f %%)\n', occurenceV(value), occurenceN(value), size(data,2), occurenceN(value)/size(data,2)*100);
        end
        fprintf('\n');
    end

% 
% occurence = size(inp(inp(row, :)==modalV(:)), 2);
% samplesAmount = size(inp(row, :), 2);
% fprintf('Occurence:\t%d / %d', occurence, samplesAmount);

    

    %fprintf('Every sample is Greater Than: %s\n', everySampleLesserThan{row});
end

