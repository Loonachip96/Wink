function row = dataset_getRecord(dataSet, rowNumber)
    fields = fieldnames(dataSet);
    fieldsAmount = numel(fields);

    for i=1:fieldsAmount
        fieldName = fields{i};
        column = dataset_getColumn(dataSet, fieldName);
        row{i} = column(rowNumber, :);
    end
end

