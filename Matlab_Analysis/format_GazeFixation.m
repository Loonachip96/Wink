function newBuffer = format_GazeFixation(lineBuffer, fixation)
    bufferLength = size(lineBuffer, 2);
    newBuffer = lineBuffer;
    
    lineString = 'Gaze Fixation:';
    newBuffer{bufferLength+1} = lineString(:);
    
    lineString = sprintf('   %f\n', fixation);
    newBuffer{bufferLength+2} = lineString(:);
    
end

