function column = dataset_getColumn(dataSet, columnName)
    column = [];
    fields = fieldnames(dataSet);
    fieldsAmount = numel(fields);
    
    for i=1:fieldsAmount
        if (strcmp(fields{i}, columnName))
            column = dataSet.(fields{i});
        end
    end
end

