function [] = headPositionPlot(x, y, z, w)
    hold on;
    grid on;
    axis([-1 1 -1 1 -1 1]);
    plot3([-1 1],[0 0],[0 0], 'y');
    plot3([0 0],[-1 1],[0 0], 'g');
    plot3([0 0],[0 0],[-1 1], 'b');
    
    carthesian = euler(quaternion([x, y, z, w]),'XYZ','point');
    Xs = carthesian(:,1);
    Ys = carthesian(:,2);
    Zs = carthesian(:,3);
    Xs = 2 * (x.*z + w.*y);
    Ys = 2 * (y.*z - w.*x);
    Zs = 1 - 2 * (x.^2 + y.^2);
    Xs = 2 * (x.*y - w.*z);
    Ys = 1 - 2 * (x.*x + z.*z);
    Zs = 2 * (y.*z + w.*x);
    
    
    for i=1:size(Xs,1)
       plot3([0 Xs(i)], [0 Ys(i)], [0 Zs(i)], 'r');
    end
    grid off;
    hold off;
end

