function heatmap = getHeatmap(Xs, Ys, brushRatio, brushIntensivity, sizeX, sizeY)
    %B G R
    brush = getBrush(brushRatio);
    stamp = brush * brushIntensivity;
    stampSize = 2*brushRatio+1;
    
    X = Xs;%abs(Xs/max(Xs));
    Y = Ys;%abs(Ys/max(Ys));
    
    Xpixels = floor(X*sizeX);
    Ypixels = floor(Y*sizeY);
        
    offset = brushRatio+1;
    
    m1 = zeros(offset, offset, 1);
    m2 = zeros(offset, sizeX, 1);
    m3 = zeros(offset, offset, 1);
    m4 = zeros(sizeY, offset, 1);
    im = zeros(sizeY, sizeX, 1);
    m6 = zeros(sizeY, offset, 1);
    m7 = zeros(offset, offset, 1);
    m8 = zeros(offset, sizeX, 1);
    m9 = zeros(offset, offset, 1);
    
    matrix = [ m1 m2 m3;
               m4 im m6;
               m7 m8 m9];
    
    for pixel=1:size(Ypixels,1)
        startX = Xpixels(pixel)+offset-brushRatio;
        startY = Ypixels(pixel)+offset-brushRatio;
        imageXStampRange = startX:startX+stampSize-1;
        imageYStampRange = startY:startY+stampSize-1;
        matrix(imageXStampRange, imageYStampRange, 1) = matrix(imageXStampRange, imageYStampRange, 1) + stamp(:,:);
    end
    
    heatmap = matrix(sizeY+offset:-1:offset, offset:sizeX+offset, 1);
end

