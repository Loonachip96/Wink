function [result] = roundNumberTo(number, rounder)
    zeroizeRounder = 0.0000000001;

    if(rounder > 0)
        result = zeros(size(number));

        distToLeft = mod(number, rounder);
        distToRight = rounder - mod(number, rounder);

        result(distToLeft >= distToRight) = number(distToLeft >= distToRight) + distToRight(distToLeft >= distToRight);
        result(distToLeft < distToRight) = number(distToLeft < distToRight) - distToLeft(distToLeft < distToRight);
        
        toBeZeroized = and(result < zeroizeRounder, result > -zeroizeRounder);        
        result(toBeZeroized) = 0;
    else
        result = number;
    end
end

