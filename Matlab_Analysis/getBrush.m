function matrix = getBrush(ratio)
    midPoint = ratio+1;
    brushLength = 2*ratio+1;
    matrix = zeros(brushLength);
    brushForce = linspace(1, 0, ratio+1);
    
    for x = 1:brushLength
        for y = 1:brushLength
            distanceFromMidPoint = ceil(sqrt((x-midPoint).^2 + (y-midPoint).^2));
            if(distanceFromMidPoint == 0)
                distanceFromMidPoint = 1;
            end
            if(distanceFromMidPoint <= ratio)
                matrix(x, y) = brushForce(distanceFromMidPoint);
            else
                matrix(x, y) = 0;
            end
        end
    end
    
end

