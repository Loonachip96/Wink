function [result data] = showResults(name, surname, gesture, attemptsNumber, NPCPosition, NPCNamesPrefixes)
    
    % 'NameSurname_GESTURE_attempt.csv'    
    filenameBeginning = strcat(name, surname, '_', gesture, '_');
    
    filenames = cell(attemptsNumber, 1);
    for i=1:attemptsNumber
        filenames{i} = strcat(filenameBeginning, num2str(i), '.csv');
    end
    
    
    datasets = cell(attemptsNumber, 1);
    for i=1:attemptsNumber
        datasets{i} = dataset_loadData(filenames{i});
    end
    
    title = strcat(gesture, ' - ', name, ' ', surname);
    [result data] = resultFormat_createResult(title, datasets, NPCPosition, NPCNamesPrefixes);
end

