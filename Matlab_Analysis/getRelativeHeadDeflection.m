function [defLR defFB rot] = getRelativeHeadDeflection(x, y, z, w, playerPos, NPCPos)
    
    iX = 1;
    iY = 2;
    iZ = 3;
    
    q = quaternion([w x y z]);
    rotations = rotm2axang(quat2rotm(q));
    angle = rad2deg(rotations(:, 4));
    
    distanceInXs = abs(NPCPos(iX) - playerPos(:, iX));
    distanceInYs = abs(NPCPos(iY) - playerPos(:, iY));
    distanceInZs = abs(NPCPos(iZ) - playerPos(:, iZ));
    distanceFromNPC = sqrt(distanceInXs.^2 + distanceInZs.^2);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% rotAroundX %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    correctionAngleOverXToFaceNPC = rad2deg(atan(distanceInYs./distanceFromNPC));
    correctionAngleOverXToFaceNPC(NPCPos(iY) > playerPos(:, iY)) = -correctionAngleOverXToFaceNPC(NPCPos(iY) > playerPos(:, iY));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% rotAroundY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    correctionAngleOverYToFaceNPC = zeros(size(playerPos,1), 1);
    trigComponent = distanceInXs ./ distanceInZs;
    
    npcInFront = rad2deg(atan(trigComponent));
    npcBehind = 90 + rad2deg(acot(trigComponent));
    
    correctionAngleOverYToFaceNPC(NPCPos(iZ) > playerPos(:, iZ)) = npcInFront(NPCPos(iZ) > playerPos(:, iZ));
    correctionAngleOverYToFaceNPC(NPCPos(iZ) <= playerPos(:, iZ)) = npcBehind(NPCPos(iZ) <= playerPos(:, iZ));
    correctionAngleOverYToFaceNPC(NPCPos(iX) < playerPos(:, iX)) = -correctionAngleOverYToFaceNPC(NPCPos(iX) < playerPos(:, iX));
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% rotAroundZ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % There is no need to compensate rotation around Z, since player and NPCs always
    % stands vertically; doesnt lean neither to left, nor right
    
        
    rotAroundX = rotations(:, 1) .* angle - correctionAngleOverXToFaceNPC;
    rotAroundY = rotations(:, 2) .* angle - correctionAngleOverYToFaceNPC;
    rotAroundZ = rotations(:, 3) .* angle;
    
    defFB = rotAroundX;
    rot = rotAroundY;
    defLR = rotAroundZ;
end

