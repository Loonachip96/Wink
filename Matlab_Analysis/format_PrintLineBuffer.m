function [] = format_PrintLineBuffer(lineBuffer)
    lineHeight = 0.12;
    linesAmount = size(lineBuffer, 2);
    
    lineHeights = 1:-lineHeight:1-linesAmount*lineHeight;
    
    for line=1:linesAmount
        text(0.5, lineHeights(line), lineBuffer{line}');
    end
end

