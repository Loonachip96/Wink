%For data series: 
%    inp = [1 2 3 4 5 6];   and row = 1
%For data series ordered in columns: 
%    inp = [1 2 3; 4 5 6]';   and row = n
clc
inp = [-45 4 -2 45 4 2; -45 4 -1 45 4 1; -45 5 -1 45 5 1; -50 3 -5 50 3 5; -50 5 -8 50 5 8; -50 3 -5 50 3 5; -50 1 -10 50 1 10]';
inp = roundNumberTo(inp, 5);

quantisatons = {0.1 0.1 0.1 0.1 0.1 0.1};
titlesOfDataRows = {
    'Forw./backw. deflection', 
    'Deflection to left/right',
    'Rotation (c.clockwise)',
    'Absolute forw./backw. deflection', 
    'Absolute deflection to left/right',
    'Absolute rotation'
    };

printStatistics(inp, quantisatons, titlesOfDataRows);